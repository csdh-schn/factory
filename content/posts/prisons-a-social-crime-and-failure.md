+++
chapter = "Part 4"
title = "Prisons: A Social Crime and Failure"
weight = 4

+++
In 1849, Feodor Dostoyevsky wrote on the wall of his prison cell the following story of THE PRIEST AND THE DEVIL:

"'Hello, you little fat father!' the devil said to the priest. 'What made you lie so to those poor, misled people? What tortures of hell did you depict? Don't you know they are already suffering the tortures of hell in their earthly lives? Don't you know that you and the authorities of the State are my representatives on earth? It is you that make them suffer the pains of hell with which you threaten them. Don't you know this? Well, then, come with me!'

"The devil grabbed the priest by the collar, lifted him high in the air, and carried him to a factory, to an iron foundry. He saw the workmen there running and hurrying to and fro, and toiling in the scorching heat. Very soon the thick, heavy air and the heat are too much for the priest. With tears in his eyes, he pleads with the devil: 'Let me go! Let me leave this hell!'

"'Oh, my dear friend, I must show you many more places.' The devil gets hold of him again and drags him off to a farm. There he sees workmen threshing the grain. The dust and heat are insufferable. The overseer carries a knout, and unmercifully beats anyone who falls to the ground overcome by hard toil or hunger.

"Next the priest is taken to the huts where these same workers live with their families--dirty, cold, smoky, ill-smelling holes. The devil grins. He points out the poverty and hardships which are at home here.

"'Well, isn't this enough?' he asks. And it seems as if even he, the devil, pities the people. The pious servant of God can hardly bear it. With uplifted hands he begs: 'Let me go away from here. Yes, yes! This is hell on earth!'

"'Well, then, you see. And you still promise them another hell. You torment them, torture them to death mentally when they are already all but dead physically! Come on! I will show you one more hell--one more, the very worst.'

"He took him to a prison and showed him a dungeon, with its foul air and the many human forms, robbed of all health and energy, lying on the floor, covered with vermin that were devouring their poor, naked, emaciated bodies.

"'Take off your silken clothes,' said the devil to the priest, 'put on your ankles heavy chains such as these unfortunates wear; lie down on the cold and filthy floor--and then talk to them about a hell that still awaits them!'

"'No, no!' answered the priest, 'I cannot think of anything more dreadful than this. I entreat you, let me go away from here!'

"'Yes, this is hell. There can be no worse hell than this. Did you not know it? Did you not know that these men and women whom you are frightening with the picture of a hell hereafter--did you not know that they are in hell right here, before they die?'"  
This was written fifty years ago in dark Russia, on the wall of one of the most horrible prisons. Yet who can deny that the same applies with equal force to the present time, even to American prisons?

With all our boasted reforms, our great social changes, and our far-reaching discoveries, human beings continue to be sent to the worst of hells, wherein they are outraged, degraded, and tortured, that society may be "protected" from the phantoms of its own making.

Prison, a social protection? What monstrous mind ever conceived such an idea? Just as well say that health can be promoted by a widespread contagion.

After eighteen months of horror in an English prison, Oscar Wilde gave to the world his great masterpiece, THE BALLAD OF READING GOAL:

> The vilest deeds, like poison weeds,  
>
> Bloom well in prison air;  
>
> It is only what is good in Man  
>
> That wastes and withers there.  Pale Anguish keeps the heavy gate,  
>
> And the Warder is Despair.
